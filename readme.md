# js で画像を preload するテスト

[demo](https://yt-practice.gitlab.io/image-reload-20211005/)

## 動く

```js
const img = new Image()
img.src = src
await new Promise(ok => (img.onload = ok))
```

# 動く

```js
const lnk = document.createElement('img')
lnk.src = src
return new Promise(ok => (lnk.onload = ok))
```

## 動く

```js
const lnk = document.createElement('link')
lnk.rel = 'preload'
lnk.as = 'image'
lnk.href = src
document.head.appendChild(lnk)
await new Promise(ok => (lnk.onload = ok))
document.head.removeChild(lnk)
```

## 動かない…

```js
const lnk = document.createElement('link')
lnk.rel = 'preload'
lnk.as = 'image'
lnk.href = src
await new Promise(ok => (lnk.onload = ok))
```

## 動かない…2

```js
const lnk = document.createElement('link')
lnk.rel = 'preload'
lnk.as = 'fetch'
lnk.href = src
await new Promise(ok => (lnk.onload = ok))
```
